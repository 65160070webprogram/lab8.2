import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order = new Order();
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    for (const oi of createOrderDto.orderItems) {
      const orderitem = new OrderItem();

      orderitem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderitem.name = orderitem.product.name;
      orderitem.price = orderitem.product.price;
      orderitem.qty = oi.qty;
      orderitem.total = orderitem.price * orderitem.qty;
      await this.orderItemsRepository.save(orderitem);
      order.orderItems.push(orderitem);
      order.total += orderitem.total;
      order.qty += orderitem.qty;
    }
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({});
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: { orderItems: true },
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
